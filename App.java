import java.util.Arrays;

public class App {
    public static void main(String[] args){
        int K = 3;
        int[] t = {100,2,3,4,5,6,7,8,9,10};
        System.out.println(tejedores(K,t));
    }
    /* sacamos el promedio de tiempo que cuesta hacer todos los paños, después los repartimos
    de tal forma que no excedan el promedio, por último retornamos el tiempo maximo, ya que como el resto
    no pueden ayudarlo, es el minimo que se necesita para completar el trabajo
     */
    public static int tejedores(int k, int[] t){
        int total = 0;
        int promedio;
        int trabajador_actual = 0;
        int[] k_t = new int[k];
        //llenamos el array de 0 para evitar problemas de logica
        Arrays.fill(k_t,0);
        //calcular el tiempo total que cuesta el proyecto para un individuo
        for (int tiempo: t) {
            total+=tiempo;
        }
        //dividir el trabajo de forma equitativa para los empleados
        promedio = total / k;
        //recorremos todos los tiempos
        for(int i=0; i<t.length;i++) {
            //al trabajador actual le sumo el tiempo de la proxima bandera
            k_t[trabajador_actual]+=t[i];
            //ahora si no estamos en la última bandera y con la siguiente bandera, superamos el promedio,
            //empezamos a asignar al siguiente
            if(i!=t.length-1 && k_t[trabajador_actual]+t[i+1]>promedio){
                trabajador_actual++;

            }
        }
        Arrays.sort(k_t);
        //Retornamos el tiempo maximo (el último del array)
        return k_t[k-1];
    }
}
